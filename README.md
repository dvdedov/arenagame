# ArenaGame
Simple Arena Shooter. Survive as long as you can and pick up bonuses for this goal.

![Game Level Screenshot](https://gitlab.com/dvdedov/arenagame/-/blob/master/ArenaGame.jpg)

Developed with Unreal Engine 4
